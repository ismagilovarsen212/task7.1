#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <algorithm>
#include <signal.h>
#include <set>

#define MAXSTRLEN 1024
#define MAXNUM 100
char num[MAXNUM];
int sock;

void SigHndlr(int s)
{
    shutdown(sock, 2);
    close(sock);
    printf("***Client completes***\n");
    exit(1);
}

int sendall(int s, char *buf, int len, int flags)
{
    int total = 0;
    int n;

    while(total < len)
    {
        n = send(s, buf+total, len-total, flags);
        if(n == -1) { break; }
        total += n;
    }

    return (n==-1 ? -1 : total);
}

int main()
{

    signal(SIGINT, SigHndlr);
    signal(SIGTSTP, SigHndlr);

    struct sockaddr_in addr;
    timeval timeout;
    sock = socket(AF_INET, SOCK_STREAM, 0);
    char buf[MAXSTRLEN];
    char message[MAXSTRLEN];
    if(sock < 0)
    {
        perror("socket");
        exit(1);
    }

    int enable = 1;

    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0){
        perror("setsockopt(SO_REUSEADDR) failed");
        exit(1);
    }

    int hto;
    printf("Enter of server's port's number: ");
    scanf("%d", &hto);
    printf("\n");
    addr.sin_family = AF_INET; // семейство AF_INET 
    addr.sin_port = htons(hto); // выбираем порт
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

    if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("connect");
        exit(2);
    }

    fd_set readset;

    do{ 
        fflush(stdout);
        FD_ZERO(&readset);
        FD_SET(sock , &readset);
        FD_SET(0, &readset);

        timeout.tv_sec = 100;
        timeout.tv_usec = 0;
        
        if(select(sock + 1, &readset, NULL, NULL, &timeout) <= 0)
        {
            perror("select");
            exit(1);
        }

        if(FD_ISSET(0, &readset))
        {
       	    printf("Tell me your wish. 1 - Volkswagen, 2 - Toyota, 3 - Audi, 4 - Mercedes, 5 - BMW, q - exit: ");
            scanf("%s", message);
            if(!strcmp(message, "q"))
                break;
            else if(!strcmp(message, "BUY") || !strcmp(message, "SELL"))
            {
                scanf("%s", num);
                sendall(sock, message, MAXSTRLEN, 0);
                sendall(sock, num, MAXNUM, 0);
            }
            else{
                sendall(sock, message, MAXSTRLEN, 0);                
            }
            
        }

        if(FD_ISSET(sock, &readset))
        {
            recv(sock, buf, sizeof(buf), 0);
            printf("\n%s\n", buf);
        } 
        
    }while(1);
    FD_CLR(0, &readset);
    FD_CLR(sock, &readset);
    shutdown(sock, 2);
    close(sock);
    return 0;
}
