#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <algorithm>
#include <signal.h>
#include <set>

#define MAXSTRLEN 1024
#define MAXNUM 100
#define CARS 5

using namespace std;

std::set<int> clients;
int listener;

int sendall(int s, char *buf, int len, int flags)
{
    int total = 0;
    int n;

    while(total < len)
    {
        n = send(s, buf+total, len-total, flags);
        if(n == -1) { break; }
        total += n;
    }

    return (n==-1 ? -1 : total);
}

void SigHndlr(int s)
{
    for(set<int>::iterator it = clients.begin(); it != clients.end(); it++)
    {
        shutdown(*it, 2);
        close(*it);
    }
    shutdown(listener, 2);
    close(listener);
    printf("***Server completes***\n");
    exit(1);
}

int main()
{
    struct sockaddr_in addr;
    char buf[MAXSTRLEN];
    int bytes_read;
    char num[MAXNUM];
    int autocar[CARS];

    signal(SIGINT, SigHndlr);
    signal(SIGTSTP, SigHndlr);

    listener = socket(AF_INET, SOCK_STREAM, 0);
    if(listener < 0)
    {
        perror("socket");
        exit(1);
    }

    fcntl(listener, F_SETFL, O_NONBLOCK);

    int enable = 1;

    if (setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0){
        perror("setsockopt(SO_REUSEADDR) failed");
        exit(1);
    }
    
    fcntl(listener, F_SETFL, O_NONBLOCK);
    
    int hto;
    printf("Enter of port's number: ");
    scanf("%d", &hto);
    printf("\n");

    addr.sin_family = AF_INET;
    addr.sin_port = htons(hto);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        exit(2);
    }

    listen(listener, 5);

    clients.clear();

    for(int i = 0; i < CARS; i++){
        printf("Enter an amount of car №%d: ", i + 1);
        scanf("%d", &autocar[i]);
    }

    do
    {
        // Заполняем множество сокетов
        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(listener, &readfds);

        for(set<int>::iterator it = clients.begin(); it != clients.end(); it++)
            FD_SET(*it, &readfds);

        // Задаём таймаут
        timeval timeout;
        timeout.tv_sec = 60;
        timeout.tv_usec = 0;

        // Ждём события в одном из сокетов
        int mx = max(listener, *max_element(clients.begin(), clients.end()));
        if(select(mx+1, &readfds, NULL, NULL, &timeout) <= 0)
        {
            perror("select");
            exit(3);
        }
        
        if(FD_ISSET(listener, &readfds))
        {
            // Поступил новый запрос на соединение, используем accept
            int sock = accept(listener, NULL, NULL);
            printf("\nHello! I'm glad to see you!\n");
            if(sock < 0)
            {
                perror("accept");
                exit(3);
            }
            
            fcntl(sock, F_SETFL, O_NONBLOCK);

            clients.insert(sock);
        }

        for(set<int>::iterator it = clients.begin(); it != clients.end(); it++)
        {
            if(FD_ISSET(*it, &readfds))
            {
                    // Поступили данные от клиента, читаем их
                    bytes_read = recv(*it, buf, MAXSTRLEN, 0);
                    if(bytes_read > 0)
                    {
                        if(!strcmp(buf, "BUY"))
                        {
                            recv(*it, num, MAXNUM, 0);
                            int numhelp = atoi(num);
                            if(!autocar[numhelp - 1])
                            {
                                strcpy(buf, "I'm sorry! We don't have this car.");
                                // Отправляем данные обратно клиенту
                                sendall(*it, buf, MAXSTRLEN, 0);
                            }
                            else
                            {
                                strcpy(buf, "Attention! A car №");
                                strncat(buf, num, MAXNUM);
                                strncat(buf, " was bought.", MAXNUM);
                                autocar[numhelp - 1]--;
                                for(set<int>::iterator ith = clients.begin(); ith != clients.end(); ith++)
                                    if(ith != it)sendall(*ith, buf, bytes_read, 0);
                                strncat(buf, "\nCongratulations! You bought car №", MAXSTRLEN);
                                strncat(buf, num , MAXNUM);
                                // Отправляем данные обратно клиенту
                                sendall(*it, buf, MAXSTRLEN, 0);
                            }
                        }
                        else if(!strcmp(buf, "SELL"))
                        {
                            recv(*it, num, MAXNUM, 0);
                            int numhelp = atoi(num);
                            strcpy(buf, "Attention! A car №");
                            strncat(buf, num, MAXNUM);
                            strncat(buf, " was sold.", MAXNUM);
                            autocar[numhelp - 1]++;
                            for(set<int>::iterator ith = clients.begin(); ith != clients.end(); ith++)
                                    if(ith != it)sendall(*ith, buf, bytes_read, 0);
                            strncat(buf, "\nCongratulations! You sold car №", MAXSTRLEN);
                            strncat(buf, num , MAXNUM);
                            // Отправляем данные обратно клиенту
                            sendall(*it, buf, MAXSTRLEN, 0);
                        }
                        else
                        {
                            strcpy(buf, "Irregular command.\n");
                            sendall(*it, buf, MAXSTRLEN, 0);
                        }
                    }

                    if(bytes_read <= 0)
                    {
                        // Соединение разорвано, удаляем сокет из множества
                        FD_CLR(*it, &readfds);
                        printf("\nGood Bye!\n");
                        shutdown(*it, 2);
                        close(*it);
                        clients.erase(*it);
                        continue;
                    }
                    
            }
        }
    }while(!clients.empty());

    printf("###The autosalon is over\n");

    shutdown(listener, 2);
    close(listener);
    
    return 0;
}